//
//  YDRegistrationViewController.m
//  MyPersonalLibrary
//
//  Created by 林良劲 on 01/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import "YDRegistrationViewController.h"
#import "KeychainItemWrapper.h"
#import "NSString+MD5.h"

@interface YDRegistrationViewController ()

@end

@implementation YDRegistrationViewController
@synthesize delegate;

-(id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle
{
    self = [super initWithNibName:nibName bundle:nibBundle];
    if(self)
    {
        
    }
    return self;
}

-(id)initWithDelegate:(id<YDRegistrationViewControllerDelegate>)delegate
{
    self = [[YDRegistrationViewController alloc] init];
    if(self)
    {
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"RegistrationVC Did Load");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//用户注册操作
-(IBAction)registerUser:(UIButton *)sender
{
//    用户名或者密码为空
    if([self.nameField.text length] == 0 || [self.passwordField.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Register Error" message:@"Both field are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    else
    {
        KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"YDAPPNAME" accessGroup:nil];
        
        [keychain setObject:self.nameField.text forKey:(__bridge id)kSecAttrAccount];
        [keychain setObject:[self.passwordField.text MD5] forKey:(__bridge id)kSecValueData];
        
        [YDConfigurationHelper setBoolValueForConfigurationKey:bYDRegistered withValue:YES];
        NSLog(@"Register Successed.");
        NSLog(@"UserName:%@", self.nameField.text);
        NSLog(@"Password:%@", self.passwordField.text);
        [self.delegate registerWithSuccess];
    }
}

-(IBAction)cancelRegistration:(UIButton *)sender
{
    [self.delegate cancelRegistration];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
