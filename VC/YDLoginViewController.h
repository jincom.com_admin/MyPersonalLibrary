//
//  YDLoginViewController.h
//  MyPersonalLibrary
//
//  Created by 林良劲 on 01/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YDLoginViewControllerDelegate<NSObject>
//登陆成功
-(void)loginWithSuccess;
//登陆失败
-(void)loginWithError;
//取消登陆
-(void)cancelLogin;

@end

@interface YDLoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordFiedl;
@property (assign, nonatomic) id<YDLoginViewControllerDelegate> delegate;

-(id)initWithDelegate:(id<YDLoginViewControllerDelegate>)delegate;
-(IBAction)loginUser:(UIButton *)sender;
-(IBAction)cancelLogin:(UIButton *)sender;

@end
