//
//  YDLoginViewController.m
//  MyPersonalLibrary
//
//  Created by 林良劲 on 01/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import "YDLoginViewController.h"
#import "NSString+MD5.h"
#import "KeychainItemWrapper.h"

@interface YDLoginViewController ()

@end

@implementation YDLoginViewController
@synthesize delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){}
    return self;
}

-(id)initWithDelegate:(id<YDLoginViewControllerDelegate>)delegate
{
    self = [[YDLoginViewController alloc] init];
    if(self)
    {
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"LoginVC Did Load");
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loginUser:(UIButton *)sender
{
    UITextField *inputName = self.nameField;
    UITextField *inputPassword = self.passwordFiedl;
    
    if([inputName.text length] == 0 || inputPassword.text.length == 0)
    {
        [self showErrorWithMessage:@"Both Filed are mandatory!"];
    }
    else
    {
        KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"YDAPPNAME" accessGroup:nil];
        
        NSString *name = [keychain objectForKey:(__bridge id)kSecAttrAccount];
        NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
        
        if([name isEqualToString:inputName.text])
        {
            if([password isEqualToString:[inputPassword.text MD5]])
            {
                [self.delegate loginWithSuccess];
            }
            else
            {
                [self.delegate loginWithError];
                [self showErrorWithMessage:@"Password not correct."];
            }
        }
        else
        {
            [self showErrorWithMessage:@"Name not exits"];
            [self.delegate loginWithError];
        }
    }
}

-(IBAction)cancelLogin:(UIButton *)sender
{
    [self.delegate cancelLogin];
}

-(void)showErrorWithMessage:(NSString *)msg
{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"Login Error" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"", nil];
    
    [alter show];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
