//
//  YDRegistrationViewController.h
//  MyPersonalLibrary
//
//  Created by 林良劲 on 01/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YDRegistrationViewControllerDelegate<NSObject>
//注册成功回调
-(void)registerWithSuccess;
//注册失败回调
-(void)registerWithError;
//取消注册回调
-(void)cancelRegistration;

@end

@interface YDRegistrationViewController : UIViewController

@property (nonatomic, assign) id<YDRegistrationViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UITextField *nameField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;

-(id)initWithDelegate:(id<YDRegistrationViewControllerDelegate>) delegate;
-(IBAction)registerUser:(UIButton *)sender;
-(IBAction)cancelRegistration:(UIButton *)sender;

@end
