//
//  YDCrashHandler.m
//  MyPersonalLibrary
//
//  Created by 林良劲 on 02/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import "YDCrashHandler.h"
#import <execinfo.h>
#import <libkern/OSAtomic.h>

NSString * const YDCrashHandlerSignalExceptionName = @"YDCrashHandlerSignalExceptionName";
NSString * const YDCrashHandlerSignalExceptionKey = @"YDCrashHandlerSignalExceptionKey";
NSString * const YDCrashHandlerAddressKey = @"YDCrashHandlerAddressKey";

volatile int32_t UncaughtExceptionCount = 0;
const int32_t UncaughtExceptionMaximum = 10;

const NSInteger UncaughtExceptionHandlerAdressCount = 4;
const NSInteger UncaughtExceptionHandlerReportAddressCount = 5;

@implementation YDCrashHandler

//获取堆栈信息，返回一个字符串数组
+(NSArray *)backtrace
{
    void* calltrack[128];
    int frames = backtrace(calltrack, 128);
    
    char **strs = backtrace_symbols(calltrack, frames);
    NSMutableArray *backtrace = [[NSMutableArray alloc] initWithCapacity:frames];
    
    int i;
    for(i = 1; i < UncaughtExceptionHandlerAdressCount + UncaughtExceptionHandlerReportAddressCount; i++)
    {
        [backtrace addObject:[[NSString alloc] initWithUTF8String:strs[i]]];
    }
    
    free(strs);
    return backtrace;
}
//AlerView回调函数
-(void)alertView:(UIAlertView *)anAlerView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    dismissed = true;
}
//处理异常
-(void)handlerException:(NSException *)exception
{
//    弹出提示框
    UIAlertView *thisAlertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"An unexpected event happend causing the application to shudown." delegate:self cancelButtonTitle:@"" otherButtonTitles:nil, nil];
    
    [thisAlertView show];
    
    CFRunLoopRef runLoop = CFRunLoopGetCurrent();
    CFArrayRef allModes = CFRunLoopCopyAllModes(runLoop);
    
    NSArray *modesArray = (__bridge NSArray *)allModes;
    //alerview弹出后dismissed为true，终结循环
    while(!dismissed)
    {
        for(NSString *mode in modesArray)
        {
            //当接受到异常处理的时候，让程序开始runLoop，防止程序死亡
            CFRunLoopRunInMode((CFStringRef)CFBridgingRetain(mode), 0.001, false);
        }
    }
    
    CFRelease(allModes);
    //把异常处理回调函数重新设置为默认
    NSSetUncaughtExceptionHandler(NULL);
    signal(SIGABRT, SIG_DFL);
    signal(SIGILL, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGBUS, SIG_DFL);
    signal(SIGPIPE, SIG_DFL);
    
    NSLog(@"Print calltrack.");
    NSArray *calltrack = [[exception userInfo] objectForKey:YDCrashHandlerAddressKey];
    
    for(NSString *str in calltrack)
    {
        NSLog(str);
    }
    
    if([[exception name] isEqual:YDCrashHandlerSignalExceptionName])
    {
        NSLog(@"raise exception %@", YDCrashHandlerSignalExceptionKey);
        kill(getpid(), [[[exception userInfo] objectForKey:YDCrashHandlerSignalExceptionKey] intValue]);
    }
    else
    {
        NSLog(@"raise exception %@", [exception name]);
        [exception raise];
    }
    
   
}

//处理异常
void HandlerExecption(NSException *exception)
{
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    //判断当前处理的异常数量是否超标
    if(exceptionCount > UncaughtExceptionMaximum)
        return;
    //获取异常点对战堆栈信息
    NSArray *callstack = [YDCrashHandler backtrace];
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[exception userInfo]];
    
    [userInfo setObject:callstack forKey:YDCrashHandlerAddressKey];
    //在主线程执行handlerException函数（因为子线程不能操作UI）
    [[[YDCrashHandler alloc] init] performSelectorOnMainThread:@selector(handlerException:) withObject:[NSException exceptionWithName:[exception name] reason:[exception reason] userInfo:userInfo] waitUntilDone:YES];
}

//sinal异常回调函数
void SignalHandler(int signal)
{
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    
    if(exceptionCount > UncaughtExceptionMaximum)
        return;
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:signal] forKey:YDCrashHandlerSignalExceptionKey];
    
    NSArray *callstack = [YDCrashHandler backtrace];
    [userInfo setObject:callstack forKey:YDCrashHandlerAddressKey];
    
    [[[YDCrashHandler alloc] init] performSelectorOnMainThread:@selector(handlerException:) withObject:[NSException exceptionWithName:YDCrashHandlerSignalExceptionName reason:[NSString stringWithFormat:@"Signal %d was raise.", signal] userInfo:userInfo] waitUntilDone:YES];
}

//初始化异常处理程序，自定义异常回调函数
void InstallCrashExceptionHandler()
{
    NSSetUncaughtExceptionHandler(&HandlerExecption);
    
    signal(SIGABRT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGFPE, SignalHandler);
    signal(SIGBUS, SignalHandler);
    signal(SIGPIPE, SignalHandler);
}
@end
