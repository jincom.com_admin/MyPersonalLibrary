//
//  YDCrashHandler.h
//  MyPersonalLibrary
//
//  Created by 林良劲 on 02/12/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YDCrashHandler : NSObject
{
    BOOL dismissed;
}

void InstallCrashExceptionHandler();

+(NSArray *)backtrace;

@end
