//
//  AppDelegate.m
//  MyPersonalLibrary
//
//  Created by 林良劲 on 30/11/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import "AppDelegate.h"
#import "YDConfigurationHelper.h"
#import "YDCrashHandler.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//安装异常处理程序
-(void)installCrashExceptionHandler
{
    InstallCrashExceptionHandler();
}
//显示MainVC
-(void)showMainViewController
{
    self.viewController = [[ViewController alloc] init];
    self.window.rootViewController = _viewController;
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
}

//显示LoginVC
-(void)showLoginVC
{
    self.loginVC = [[YDLoginViewController alloc] initWithDelegate:self];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    self.window.rootViewController = _loginVC;
    [self.window makeKeyAndVisible];
}

//显示RegistrationVC
-(void)showRegistrationVC
{
    //self.registrationVC = [[YDRegistrationViewController alloc] init];
    self.registrationVC = [[YDRegistrationViewController alloc] initWithDelegate:self];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    self.window.rootViewController = self.registrationVC;
    [self.window makeKeyAndVisible];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSLog(@"----->didFinishLaunchingWithOptions");
    
    //是否安装异常处理程序
    if(bYDInstallCrashHandler)
    {
        [self performSelector:@selector(installCrashExceptionHandler) withObject:nil afterDelay:0];
    }
    
    //创建UIWindow
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //APP是否第一次启动
    if([YDConfigurationHelper getBoolValueForConfigurationKey:bYDFirstLaunch])
    {
        [YDConfigurationHelper setApplicationStartupDefaults];
    }
    
    //是否启动GPS功能
    if(bYDActivateGPSOnStartUp)
    {
        
    }
    
    //如果需要注册并且还没注册过，显示注册view
    if(bYDRegistrationRequired && ![YDConfigurationHelper getBoolValueForConfigurationKey:bYDRegistered])
    {
        [self showRegistrationVC];
    }
    //显示登陆view
    else
    {
        if(bYDLoginRequired)
        {
            [self showLoginVC];
        }
        else
        {
            [self showMainViewController];
        }
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    NSLog(@"----->applicationWillResignActive");
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//取消登陆回调
- (void)cancelLogin {
    NSLog(@"cancelLogin.");
}
//登陆错误
- (void)loginWithError {
    NSLog(@"loginWithError.");
}
//登陆成功
- (void)loginWithSuccess {
    [self showMainViewController];
}
//取消注册
- (void)cancelRegistration {
    NSLog(@"cancelRegistration.");
}
//注册错误
- (void)registerWithError {
    NSLog(@"registerWithError.");
}
//注册成功
- (void)registerWithSuccess
{
    if(bYDShowLoginAfterRegistration)
    {
        [self showLoginVC];
    }
    else
    {
        [self showMainViewController];
    }
}

@end
