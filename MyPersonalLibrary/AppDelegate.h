//
//  AppDelegate.h
//  MyPersonalLibrary
//
//  Created by 林良劲 on 30/11/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YDLoginViewController.h"
#import "YDRegistrationViewController.h"

@class ViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate, YDLoginViewControllerDelegate, YDRegistrationViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) YDLoginViewController *loginVC;
@property (strong, nonatomic) YDRegistrationViewController *registrationVC;

@end

