//
//  main.m
//  MyPersonalLibrary
//
//  Created by 林良劲 on 30/11/2017.
//  Copyright © 2017 林良劲. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
